#!/usr/bin/zsh -f
set -uo pipefail

# clean up before crawl
rm -r crawl/*

# clean up
rm public/ponuka/*
rm public/index.html

# crawl lidl.sk
(
    cd crawl || { echo >&2 "failed to cd into crawl"; exit 1; }
    httrack https://www.lidl.sk/sk/index.htm -r2 "-*" "+*/sk/c/*"
)

function cleanName {
    name=${1#*/*/*/*/}
    print -- ${name//\//-}
}

# generate static sites into ponuka/
grep -E 'od (Po|Ut|St|Št|Pi|So|Ne).*</title>' -m1 -l crawl/www.lidl.sk/sk/c/**/*.html | while read file; do awk -f lidler.awk ${file} | ./lidl.zsh >public/ponuka/$(cleanName ${file}); done

# create new index.html
./index.zsh >public/index.html
