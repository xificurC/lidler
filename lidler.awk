function textOf(str) {
    sub(/^[[:space:]]*(<[^>]+\/?>)*[[:space:]]*/, "", str)
    sub(/[[:space:]]*(<[^>]+\/?>)*[[:space:]]*$/, "", str)
    return str
}

function dbg(str) {
    if (DEBUG) printf("%s \t| ", str)
}

/<title>/ && !_title {
    _title=1
    dbg("page title")
    print textOf($0)
    next
}

/(third|half)\/lg/ {
    dbg("product image")
    for (i = 1; i <= NF; i++) {
        if (match($i, /https:\/\/.*(third|half)\/lg[^,]+/) != 0) {
            print substr($i, RSTART, RLENGTH)
            break
        }}
    next
}

/product__body/ {
    dbg("product link")
    for (i = 1; i <= NF; i++) {
        if ($i ~ /^href="/) {
            $i = substr($i, 7, length($i) - 7 - 1)
            print $i
            break
        }
    }
    next
}

/product__desc/ {
    dbg("product desc")
    getline
    print textOf($0)
    next
}

/product__title/ {
    dbg("product title")
    getline
    $0=$0
    print htmlText($0)
    next
}

function htmlText(str) {
    gsub(/&nbsp;/, "", str)
    gsub(/<[^>]+>/, " ", str)
    sub(/^[[:space:]]+/, "", str)
    sub(/[[:space:]]+$/, "", str)
    gsub(/[[:space:]]+/, " ", str)
    gsub(/ \. /, ".", str)
    return str
}

/pricebox__price"/ {
    dbg("product price")
    getline
    $0=$0
    print htmlText($0)
    next
}
