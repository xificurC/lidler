#!/usr/bin/zsh -f
set -uo pipefail

function line {
    IFS= read -r ${1}
}

line title
cat <<EOF
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>${title}</title>
        <link rel="stylesheet" href="https://unpkg.com/basscss@8.0.4/css/basscss.min.css">
    </head>
    <body>
        <div class="flex flex-wrap justify-evenly">
EOF

while line link; do
    line image
    line product
    line description
    line price
    cat <<EOF
            <div class="flex flex-column items-center m2">
                <a href="${link}">
                    <img alt="${product}" src="${image}"/>
                </a>
                <div class="h3 bold">${product}</div>
                <div class="italic">${description}</div>
                <div class="underline italic">${price}</div>
            </div>
EOF
done


cat <<EOF
        </div>
    </body>
</html>
EOF
