#!/usr/bin/zsh -f
set -uo pipefail

cat <<EOF
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Lidl sk static</title>
        <link rel="stylesheet" href="https://unpkg.com/basscss@8.0.4/css/basscss.min.css">
    </head>
    <body>
        <ul class="list-reset">
EOF

cd public
grep -m1 -E '<title>' ponuka/*.html \
    | sed -r 's,</?title>,,g' \
    | while IFS= read -r line; do
    d=$(grep -oP ', \K[^ ]+' <<<${line})
    d=(${(s:.:)d})
    if [[ $#d -gt 1 ]]; then printf "%d %d %s\n" $d[2] $d[1] $line; else print "99 99 $line"; fi
done | sort -k1n -k2n -k4 \
    | awk '{$1=""; $2=""; print}' \
    | awk -F':[[:space:]]+' '
!seen[$2]++ {
    gsub(/<\/?title>/, "", $2)
    sub(/^[[:space:]]+/, "", $1)
    printf "            <li class=\"m1\"><a class=\"text-decoration-none h3\" href=\"%s\">%s</a></li>\n", $1, $2
}
'

cat <<EOF
        </ul>
    </body>
</html>
EOF
